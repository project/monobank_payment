<?php

namespace Drupal\monobank_payment\Plugin\Payment\MethodConfiguration;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\payment_offsite_api\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationBaseOffsite;

/**
 * Provides the configuration for the payment_monobank payment method plugin.
 *
 * Plugins extending this class should provide a configuration schema that
 * extends
 * plugin.plugin_configuration.payment_method_configuration.payment_monobank.
 *
 * @PaymentMethodConfiguration(
 *   description = @Translation("A payment method type that process payments via Monobank payment gateway."),
 *   id = "payment_monobank",
 *   label = @Translation("Monobank")
 * )
 */
class Monobank extends PaymentMethodConfigurationBaseOffsite implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'message_text' => 'In addition to the order amount monobank fee can be charged.',
      'message_text_format' => 'plain_text',
      'auto_submit' => TRUE,
      'verbose' => FALSE,
      'ipn_statuses' => [
        'success' => 'payment_success',
        'failure' => 'payment_failed',
        'reversed' => 'payment_refunded',
        'processing' => 'payment_pending',
        'expired' => 'payment_expired',
        'hold' => 'payment_unknown',
        'created' => 'payment_created',
      ],
      'config' => [
        'x_token' => '',
        'validity' => 3600,
        'payment_type' => 'debit',
        'action_url' => 'https://api.monobank.ua/api',
        'redirect_url' => '[redirect_to_referer]',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function processBuildConfigurationForm(array &$element, FormStateInterface $form_state, array &$form) {
    $element['x_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('X-Token'),
      '#default_value' => $this->getXToken(),
      '#required' => TRUE,
      '#description' => $this->t('Token from personal account https://fop.monobank.ua/ or test token from https://api.monobank.ua/'),
    ];
    $element['validity'] = [
      '#type' => 'number',
      '#title' => $this->t('Validity time'),
      '#default_value' => $this->getValidity(),
      '#required' => TRUE,
      '#description' => $this->t('The validity period is in seconds, by default the account expires after 24 hours'),
      '#min' => 60,
      '#attributes' => [
        'min' => '60',
      ],
    ];
    $element['payment_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Payment Type'),
      '#options' => [
        'debit' => $this->t('debit'),
        'hold' => $this->t('hold'),
      ],
      '#default_value' => $this->getPaymentType(),
      '#required' => TRUE,
    ];
    $element['action_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Action url'),
      '#default_value' => $this->getActionUrl(),
      '#required' => TRUE,
      '#description' => $this->t('The validity period is in seconds, by default the account expires after 24 hours'),
    ];
    $element['redirect_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect url'),
      '#description' => $this->t('If you have installed Token module you may use tokens in URL. Set [redirect_to_referer] if you wanna set redirection to referer page.'),
      '#default_value' => $this->getRedirect(),
      '#required' => TRUE,
    ];

    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $element['token_help'] = [
        '#type' => 'details',
        '#title' => $this->t('Available tokens'),
      ];
      $element['token_help']['tree'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['global'],
        '#element_validate' => [
          'token_element_validate',
        ],
        '#default_value' => '',
      ];
    }

    return parent::processBuildConfigurationForm($element, $form_state, $form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $parents = $form['plugin_form']['#parents'];
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $parents);

    $this->setXToken($values['x_token']);
    $this->setValidity($values['validity']);
    $this->setPaymentType($values['payment_type']);
    $this->setActionUrl($values['action_url']);
    $this->setRedirect($values['redirect_url']);
  }

  /**
   * Returns the default X-Token.
   *
   * @return string
   *   The X-Token.
   */
  public function getXToken() {
    return $this->configuration['config']['x_token'];
  }

  /**
   * Sets default X-Token.
   *
   * @param string $x_token
   *   The X-Token.
   *
   * @return static
   */
  public function setXToken($x_token) {
    $this->configuration['config']['x_token'] = $x_token;

    return $this;
  }

  /**
   * Returns the store validity.
   *
   * @return int
   *   The store validity.
   */
  public function getValidity() {
    return $this->configuration['config']['validity'];
  }

  /**
   * Sets the store validity.
   *
   * @param string $validity
   *   The store validity.
   *
   * @return static
   */
  public function setValidity($validity) {
    $this->configuration['config']['validity'] = $validity;

    return $this;
  }

  /**
   * Returns the default PaymentType.
   *
   * @return string
   *   The PaymentType.
   */
  public function getPaymentType() {
    return $this->configuration['config']['payment_type'];
  }

  /**
   * Sets default PaymentType.
   *
   * @param string $payment_type
   *   The PaymentType.
   *
   * @return static
   */
  public function setPaymentType($payment_type) {
    $this->configuration['config']['payment_type'] = $payment_type;

    return $this;
  }

  /**
   * Returns the default action url.
   *
   * @return string
   *   The action url.
   */
  public function getActionUrl() {
    return $this->configuration['config']['action_url'];
  }

  /**
   * Sets default action url.
   *
   * @param string $action_url
   *   The action url.
   *
   * @return static
   */
  public function setActionUrl($action_url) {
    $this->configuration['config']['action_url'] = $action_url;

    return $this;
  }

  /**
   * Returns redirect value.
   *
   * @return int
   *   Redirect value.
   */
  public function getRedirect() {
    return $this->configuration['config']['redirect_url'];
  }

  /**
   * Sets redirect value.
   *
   * @param string $redirect
   *   Redirect value.
   *
   * @return static
   */
  public function setRedirect($redirect) {
    $this->configuration['config']['redirect_url'] = trim($redirect);

    return $this;
  }

}
