<?php

namespace Drupal\monobank_payment\Plugin\Payment\Method;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Utility\Token;
use Drupal\payment\EventDispatcherInterface;
use Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface;
use Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodBaseOffsite;
use Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodOffsiteInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Monobank payment method.
 *
 * @PaymentMethod(
 *   id = "payment_monobank",
 *   deriver = "\Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodBaseOffsiteDeriver",
 *   operations_provider = "\Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodBaseOffsiteOperationsProvider",
 * )
 */
class Monobank extends PaymentMethodBaseOffsite implements PaymentMethodOffsiteInterface, ContainerFactoryPluginInterface {

  /**
   * The response data from Monobank Api.
   *
   * @var array
   */
  private $responseData;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The URL generator service.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cache object associated with the specified bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * Constructs a new instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\payment\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Utility\Token $token
   *   The token API.
   * @param \Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface $payment_status_manager
   *   The payment status manager.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A Guzzle client object.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The URL generator service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The requested cache bin.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, ModuleHandlerInterface $module_handler, EventDispatcherInterface $event_dispatcher, Token $token, PaymentStatusManagerInterface $payment_status_manager, ClientInterface $http_client, UrlGeneratorInterface $url_generator, EntityTypeManagerInterface $entity_type_manager, CacheBackendInterface $cache) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $module_handler,
      $event_dispatcher,
      $token,
      $payment_status_manager
    );
    $this->httpClient = $http_client;
    $this->urlGenerator = $url_generator;
    $this->entityTypeManager = $entity_type_manager;
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('payment.event_dispatcher'),
      $container->get('token'),
      $container->get('plugin.manager.payment.status'),
      $container->get('http_client'),
      $container->get('url_generator'),
      $container->get('entity_type.manager'),
      $container->get('cache.default')
    );
  }

  /**
   * Set $responseData.
   */
  public function setResponseData() {
    $response_data = file_get_contents('php://input');
    $this->responseData = Json::decode($response_data);
    if ($this->responseData && $this->isVerbose()) {
      $this->logger->info('Request data <pre>@data</pre>',
        ['@data' => print_r($response_data, TRUE)]
      );
    }
  }

  /**
   * Returns $responseData.
   *
   * @return array
   *   The $responseData.
   */
  public function getResponseData() {
    if (empty($this->responseData)) {
      $this->setResponseData();
    }
    return $this->responseData;
  }

  /**
   * Returns $responseData value by key.
   *
   * @return string
   *   The $responseData value.
   */
  public function getResponseDataValue($key) {
    $response_data = $this->getResponseData();
    if (empty($response_data[$key])) {
      return FALSE;
    }
    return $response_data[$key];
  }

  /**
   * Returns the default X-Token.
   *
   * @return string
   *   The X-Token.
   */
  public function getXToken() {
    return $this->pluginDefinition['config']['x_token'];
  }

  /**
   * Returns validity time.
   *
   * @return int
   *   The validity.
   */
  public function getValidity() {
    return $this->pluginDefinition['config']['validity'];
  }

  /**
   * Returns the default PaymentType.
   *
   * @return string
   *   The PaymentType.
   */
  public function getPaymentType() {
    return $this->pluginDefinition['config']['payment_type'];
  }

  /**
   * Returns action.
   *
   * @return string
   *   The action.
   */
  public function getActionUrl() {
    // Remove "/" at the end.
    return trim($this->pluginDefinition['config']['action_url'], " \n\r\t\v\x00\/");
  }

  /**
   * Returns redirect value.
   *
   * @return int
   *   Redirect value.
   */
  public function getRedirect() {
    return $this->pluginDefinition['config']['redirect_url'];
  }

  /**
   * Gets the status to set on payment execution.
   *
   * @param string $status_id
   *   Payment gateway status.
   *
   * @return string
   *   The plugin ID of the payment status to set.
   */
  public function getStatusId(string $status_id) {
    return $this->configuration['ipn_statuses'][$status_id];
  }

  /**
   * {@inheritdoc}
   */
  protected function getSupportedCurrencies() {
    return TRUE;
  }

  /**
   * Returns transition id name.
   *
   * @return string
   *   The transition id name.
   */
  public function getTransactionIdName() {
    return 'reference';
  }

  /**
   * Amount name getter.
   *
   * @return string
   *   Amount name.
   */
  public function getAmountName() {
    return 'amount';
  }

  /**
   * Currency name getter.
   *
   * @return string
   *   Currency name.
   */
  public function getCurrencyName() {
    return 'ccy';
  }

  /**
   * Returns the ID of the payment method this plugin is for.
   *
   * @return string
   *   The configured entity ID.
   */
  public function getEntityId() {
    return $this->payment->id();
  }

  /**
   * Returns configured currency.
   *
   * @return string
   *   The currency. Default UAH. ISO 4217 => 980
   */
  public function getCurrency() {
    $currency_number = $this->getPayment()->getCurrency()->getCurrencyNumber();
    return empty($currency_number) ? '980' : intval($currency_number);
  }

  /**
   * Returns the $_SERVER['HTTP_REFERER'].
   *
   * @return string
   *   HTTP_REFERER.
   */
  public function getHttpReferer() {
    return $this->request->server->get('HTTP_REFERER');
  }

  /**
   * {@inheritdoc}
   */
  public function isConfigured() {
    return !empty($this->getXToken());
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentStatusAccess(AccountInterface $account) {
    // @to_do check
    return AccessResult::allowed();
  }

  /**
   * Get a list of required keys.
   *
   * @return array
   *   List of required keys.
   */
  public function getRequiredKeys() {
    return [
      'invoiceId',
      'status',
      $this->getAmountName(),
      $this->getCurrencyName(),
      'createdDate',
      'modifiedDate',
      $this->getTransactionIdName(),
    ];
  }

  /**
   * Validators names array for  ipnValidateDefault helper.
   *
   * @return array
   *   Validate method names array.
   */
  protected function getValidators() {
    return [
      'validateEmpty',
      'validateResponseError',
      'validateRequiredKeys',
      'validateTransactionId',
      'validateCurrency',
      'validateAmount',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function paymentForm() {
    $form = [];
    $payment = $this->getPayment();

    $params = [
      $this->getAmountName() => intval(round($payment->getAmount(), 2) * 100),
      $this->getCurrencyName() => $this->getCurrency(),
      'merchantPaymInfo' => [
        $this->getTransactionIdName() => $this->getEntityId(),
        'destination' => $this->t('Order ID: @order_id, User mail: @mail', [
          '@order_id' => $payment->id(),
          '@mail' => $payment->getOwner()->getEmail(),
        ])->render(),
      ],
      'webHookUrl' => $this->urlGenerator->generateFromRoute(
        'payment.offsite.external',
        [
          'payment_method_configuration' => 'monobank',
          'external_status' => 'ipn',
        ],
        ['absolute' => TRUE]
      ),
      "validity" => intval($this->getValidity()),
      "paymentType" => $this->getPaymentType(),
    ];

    $redirect_url = $this->getRedirect();
    if ($redirect_url) {
      if ($redirect_url == '[redirect_to_referer]') {
        $http_referer = $this->getHttpReferer();
        $params['redirectUrl'] = empty($http_referer) ? $this->urlGenerator->generateFromRoute('<front>', [], ['absolute' => TRUE]) : $http_referer;
      }
      else {
        $params['redirectUrl'] = $this->token->replace($redirect_url, ['global']);
      }
    }

    $server_output = $this->apiRequest('api/merchant/invoice/create', $params);

    $this->setAdditionalData($server_output, TRUE);

    $this->addPaymentFormData('plugin_id', 'monobank');
    if (isset($server_output['pageUrl'])) {
      $form['#action'] = $server_output['pageUrl'];
    }
    else {
      if (empty($http_referer)) {
        $http_referer = $this->getHttpReferer();
      }
      $form['#action'] = empty($http_referer) ? $this->urlGenerator->generateFromRoute('<front>') : $http_referer;
      $form['error'] = [
        '#markup' => '<p>' . $this->t('The payment could not be processed.') . '</p>',
      ];
    }

    $form += $this->generateForm();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getResultPages() {
    return [
      'invoice' => FALSE,
    ];
  }

  /**
   * Returns info about Invoice.
   *
   * @return array
   *   The renderable array.
   */
  public function getInvoiceContent() {
    $invoiceId = $this->request->query->get('invoiceId');
    if (empty($invoiceId)) {
      $output = $this->t('Unknown invoice ID');
    }
    else {
      $action_url = 'api/merchant/invoice/status?invoiceId=' . $invoiceId;
      $server_output = $this->apiRequest($action_url);

      if ($server_output) {
        $output = [];
        $output[] = $this->t('Invoice: @invoice_id', ['@invoice_id' => $server_output['invoiceId']]);
        $output[] = $this->t('Status: @external_status.', ['@external_status' => $server_output['status']]);
        $output[] = $this->t('Amount: @amount.', ['@amount' => $server_output[$this->getAmountName()]]);
        $output = implode('<br/>', $output);
      }
      else {
        $output = $this->t('Payment error.');
      }
    }

    return [
      '#markup' => $output,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function ipnExecute() {
    if ($this->isVerbose()) {
      $this->logger->info('Run ipnExecute()');
    }

    if (!$this->ipnValidate()) {
      return [
        'status' => 'fail',
        'message' => "",
        'response_code' => 400,
      ];
    }

    $payment = $this->getPayment();

    $response_data = $this->getResponseData();

    $payment_status = $this->getStatusId($response_data['status']);
    $status = empty($payment_status) ? 'payment_pending' : $payment_status;

    $updated = FALSE;
    if ($status == 'payment_created') {
      $statuses = $payment->getPaymentStatuses();
      foreach ($statuses as $active_status) {
        if ($active_status->getPluginId() == $status) {
          $updated = TRUE;
          // $statuses[$id]->setCreated(strtotime($response_data['createdDate']));
          // $payment->setPaymentStatuses($statuses);
          $this->setAdditionalData($response_data);
          break;
        }
      }
    }
    else {
      $active_status = $payment->getPaymentStatus();
      if ($active_status->getPluginId() == $status) {
        $updated = TRUE;
        $active_status->setCreated(strtotime($response_data['modifiedDate']));
        $payment->setPaymentStatus($active_status);
      }
    }

    if (!$updated) {
      $active_status = $this->paymentStatusManager->createInstance($status);
      $active_status->setCreated(strtotime($response_data['modifiedDate']));
      $payment->setPaymentStatus($active_status);
    }

    // 'finalAmount'
    $payment->save();

    if (!$this->isFallbackMode()) {
      return [
        'status' => 'success',
        'message' => 'OK',
        'response_code' => 200,
      ];
    }

    return [];
  }

  /**
   * Send a request to Monobank.
   *
   * @return array
   *   Response data.
   */
  private function apiRequest($action_url, $post_data = NULL) {
    $action_url = $this->getActionUrl() . '/' . $action_url;

    $options = [
      'headers' => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
        'X-Token' => $this->getXToken(),
      ],
    ];

    try {
      if ($post_data) {
        $options['body'] = Json::encode($post_data);
        $request = $this->httpClient->post($action_url, $options);
      }
      else {
        $request = $this->httpClient->get($action_url, $options);
      }

      $server_json = $request->getBody()->getContents();
    }
    catch (RequestException $e) {
      // An error happened.
    }

    // Json decode.
    return empty($server_json) ? [] : Json::decode($server_json, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function ipnValidate() {
    // Initialize ResponseData.
    $this->getResponseData();

    $validators = $this->getValidators();
    foreach ($validators as $validator) {
      if (!method_exists($this, $validator)) {
        if ($this->isVerbose()) {
          $this->logger->error('Validator @method not exists',
            ['@method' => $validator]
          );
        }
        return FALSE;
      }

      if (!$this->$validator()) {
        if ($this->isVerbose()) {
          $this->logger->error('Validator @method return FALSE',
            ['@method' => $validator]
          );
        }
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Required keys default validator.
   *
   * @return bool
   *   TRUE on successful validation FALSE otherwise.
   */
  protected function validateRequiredKeys() {
    $response_data = $this->getResponseData();
    $unavailable_required_keys = array_diff($this->getRequiredKeys(), array_keys($response_data));
    if (!empty($unavailable_required_keys)) {
      if ($this->isVerbose()) {
        $this->logger->error('Missing POST keys. POST data: <pre>@data</pre>',
          ['@data' => print_r($this->getResponseData(), TRUE)]
        );
      }
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Currency default validator.
   *
   * @return bool
   *   TRUE on successful validation FALSE otherwise.
   */
  protected function validateCurrency() {
    $request_currency = $this->getResponseDataValue($this->getCurrencyName());
    $currency = $this->getCurrency();
    if ($currency != $request_currency) {
      // Convert payment to UAH.
      if ($request_currency == 980) {
        $rate = $this->getBankCurrencyRateBuy($currency);
        if ($rate) {
          if ($this->getPayment()->getAmount()) {
            $this->getPayment()->setCurrencyCode('UAH');

            $line_items = $this->getPayment()->getLineItems();
            foreach ($line_items as $key => $line_item) {
              $amount = $line_item->getAmount() * $rate;
              $line_items[$key]->setAmount($amount)->setCurrencyCode('UAH');
            }

            $this->getPayment()->setLineItems($line_items);

            return TRUE;
          }
        }
      }

      if ($this->isVerbose()) {
        $this->logger->error('Missing transaction id currency. POST data: <pre>@data</pre>',
          ['@data' => print_r($this->getResponseData(), TRUE)]
        );
      }

      return FALSE;
    }
    return TRUE;
  }

  /**
   * Transaction ID default validator.
   *
   * @return bool
   *   TRUE on successful validation FALSE otherwise.
   */
  protected function validateTransactionId() {
    $request_payment_id = $this->getResponseDataValue($this->getTransactionIdName());
    $payment = $this->entityTypeManager
      ->getStorage('payment')
      ->load($request_payment_id);
    if (!$payment) {
      if ($this->isVerbose()) {
        $this->logger->error('Missing transaction id. POST data: <pre>@data</pre>',
          ['@data' => print_r($this->getResponseData(), TRUE)]
        );
      }
      return FALSE;
    }
    $this->setPayment($payment);
    return TRUE;
  }

  /**
   * Amount default validator.
   *
   * @return bool
   *   TRUE on successful validation FALSE otherwise.
   */
  protected function validateAmount() {
    $request_amount = $this->getResponseDataValue($this->getAmountName());
    if ($this->getPayment()->getAmount() != $request_amount / 100) {
      if ($this->isVerbose()) {
        $this->logger->error('Missing transaction amount. POST data: <pre>@data</pre>',
          ['@data' => print_r($this->getResponseData(), TRUE)]
        );
      }
      return FALSE;
    }
    return TRUE;
  }

  /**
   * ResponseError default validator.
   *
   * @return bool
   *   TRUE on successful validation FALSE otherwise.
   */
  protected function validateResponseError() {
    $response_data = $this->getResponseData();

    if (!empty($response_data['errCode'])) {
      $this->setAdditionalData($response_data);
      if ($this->isVerbose()) {
        $this->logger->error('Response Error. POST data: <pre>@data</pre>',
          ['@data' => print_r($response_data, TRUE)]
        );
      }
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Empty default validator.
   *
   * @return bool
   *   TRUE on successful validation FALSE otherwise.
   */
  protected function validateEmpty() {
    $response_data = $this->getResponseData();
    // Exit now if the $_POST was empty.
    if (empty($response_data)) {
      if ($this->isVerbose()) {
        $this->logger->error('Interaction URL accessed with no POST data submitted.');
      }
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Store response to field_additional_data.
   */
  public function setAdditionalData($server_output, $clear_old = FALSE) {
    $payment = $this->getPayment();
    if ($payment->hasField('additional_data')) {
      $additional_data = $payment->get('additional_data')->getValue();
      if (!$clear_old && !empty($additional_data[0]['value'])) {
        $additional_data = unserialize($additional_data[0]['value'], ['allowed_classes' => FALSE]);
        $additional_data[] = $server_output;
      }
      else {
        $additional_data = [$server_output];
      }
      $payment->set('additional_data', serialize($additional_data));
      $payment->save();
    }
  }

  /**
   * Get actual currencies from Monobank.
   *
   * @return array
   *   Currencies data.
   */
  public function getBankCurrencies() {
    $cache_id = 'monobank_payment:bank_currencies';

    if ($cache = $this->cache->get($cache_id)) {
      return $cache->data;
    }

    $data = $this->apiRequest('bank/currency');
    // Cache life is only 5 minutes.
    $this->cache->set($cache_id, $data, time() + 300);

    return $data;
  }

  /**
   * Get currency data.
   *
   * @param mixed $currency_number
   *   Currency number in ISO format.
   *
   * @return mixed
   *   Currency data on successful and FALSE otherwise.
   */
  public function getBankCurrency($currency_number) {
    $currencies = $this->getBankCurrencies();

    $key = array_search($currency_number, array_column($currencies, 'currencyCodeA', TRUE));
    if ($key !== FALSE) {
      return $currencies[$key];
    }

    return FALSE;
  }

  /**
   * Get currency equivalent in UAH.
   *
   * @param mixed $currency_number
   *   Currency number in ISO format.
   *
   * @return mixed
   *   Exchange rate in UAH on successful and FALSE otherwise.
   */
  public function getBankCurrencyRateBuy($currency_number) {
    $currency = $this->getBankCurrency($currency_number);
    if ($currency) {
      return $currency['rateBuy'];
    }

    return FALSE;
  }

}
