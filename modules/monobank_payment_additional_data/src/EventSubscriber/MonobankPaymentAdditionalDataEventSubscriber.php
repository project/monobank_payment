<?php

namespace Drupal\monobank_payment_additional_data\EventSubscriber;

use Drupal\payment\Entity\Payment;
use Drupal\payment\Event\PaymentEvents;
use Drupal\payment\Event\PaymentStatusSet;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Event subscriptions for events dispatched by Payment.
 */
class MonobankPaymentAdditionalDataEventSubscriber implements EventSubscriberInterface {

  /**
   * Request Stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * Constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request Stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * Returns an array of event names this subscriber wants to listen to.
   *
   * @return array
   *   The event names to listen to
   */
  public static function getSubscribedEvents() {
    $events = [
      PaymentEvents::PAYMENT_STATUS_SET => 'paymentStatusSet',
    ];
    return $events;
  }

  /**
   * Event PaymentEvents::PAYMENT_STATUS_SET.
   */
  public function paymentStatusSet(PaymentStatusSet $event) {
    $payment = $event->getPayment();
    $this->setAdditionalData($payment);
  }

  /**
   * Store response to field_additional_data.
   */
  public function setAdditionalData(Payment $payment) {
    $payment_method = $payment->getPaymentMethod();
    if ($payment_method) {
      $new_data = FALSE;
      if (strpos($payment_method->getPluginId(), "payment_monobank") !== FALSE) {
        $new_data = $payment_method->getResponseData();
      }
      elseif (strpos($payment_method->getPluginId(), "payment_liqpay") !== FALSE) {
        $response_data_base_64 = $this->requestStack->getCurrentRequest()->query->get('data');
        if ($response_data_base_64) {
          $response_data_json = base64_decode($response_data_base_64);
          $new_data = Json::decode($response_data_json);
        }
      }
      else {
        $new_data = $this->requestStack->getCurrentRequest()->request;
      }

      if ($new_data) {
        $additional_data = $payment->get('additional_data')->getValue();
        if (empty($additional_data[0]['value'])) {
          $additional_data = [$new_data];
        }
        else {
          $additional_data = unserialize($additional_data[0]['value'], ['allowed_classes' => FALSE]);
          $additional_data[] = $new_data;
        }

        $payment->set('additional_data', serialize($additional_data));
      }
    }
  }

}
