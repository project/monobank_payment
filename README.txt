Monobank Payment

This module provide Payment( https://www.drupal.org/project/payment )
integration with Monobank( https://www.monobank.ua ) payment system.

Registering with Monobank
Before you start the installation process you must register
on Monobank( https://www.monobank.ua ).
You will get "X-Token" and other settings for your payment system.

Dependencies:
Payment Offsite API(https://www.drupal.org/project/payment_offsite_api) with paths
- https://www.drupal.org/project/payment_offsite_api/issues/3420193
- https://www.drupal.org/project/payment_offsite_api/issues/3317655

Installation and Configuration
1) Download the module from Drupal.org and extract it to your modules folder.
2) Enable it.
3) Go to /admin/config/services/payment/method
and enable Monobank payment method configuration.
4) Edit Payment method configuration.
5) Setup the settings according to your data from Monobank.
That's it :)

Additional information:
Default Interaction url
 - http://example.com/payment_offsite/monobank/ipn
Invoice information url
 - http://example.com/payment_offsite/monobank/invoice?invoiceId=INVOICE_ID

Modules
Monobank Payment Additional Data
- added field "additional_data" to Payment entity
and stored all requests from payment system.

More API documentation can be found at Monobank API
( https://api.monobank.ua/docs/acquiring.html ).

Supporting organizations:
Diya(https://diya.pro)
